# Project Details

### Authors

- Tom Bradshaw
- Donovan Carthew
- Jonathan Coyle
- Jonathon Hope
- Aaron Murnain

### Description

An attempt at an implementation and associated suite of tests parameterised approximation algorithm for minimum vertex cover.

[Available here.](http://www.sciencedirect.com/science/article/pii/S0304397512010821)

### Disclaimer

This was undertaken as part of an honours course, COMP4130, at the University of Newcastle, under the supervision of [Prof. Ljiljana Brankovic](https://www.newcastle.edu.au/profile/ljiljana-brankovic).

### Running

This project uses gradle. This makes it easy to import into IntelliJ, Exclipse or Netbeans. 
However, make sure you do not push any platform specific files into the codebase.