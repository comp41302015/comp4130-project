/**
 * Copyright(C) 2015 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package org.comp4130.algorithm;

import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.jgrapht.Graph;

/**
 * A parameterised factor (2l + 1) / (l + 1) approximation algorithm for minimum vertex cover.
 *
 * @author Jonathon Hope
 */
public class Algorithm2 implements Algorithm {

    /**
     * {@inheritDoc}
     */
    @Override
    public Result apply(Graph<Vertex, Edge> vertexEdgeGraph) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

}