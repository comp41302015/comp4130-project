/**
 * Copyright(C) 2015 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package org.comp4130.algorithm;

import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.jgrapht.Graph;

import java.util.function.Function;

/**
 * Specifies the general algorithm contract as a standard JDK8 {@link java.util.function.Function}
 * that takes a graph as a parameter and returns a Result.
 *
 * @author Jonathon Hope
 */
public interface Algorithm extends Function<Graph<Vertex, Edge>, Result> {

}
