package org.comp4130.algorithm;

/**
 * A Result wraps the return type of the execution of an algorithm. Perhaps useful
 * statistics could be recorded for benchmarking, and presumably finding the
 * {@code Set<Vertex>} as the minimum vertex cover.
 *
 * @author Jonathon Hope
 */
public interface Result {

    // TODO specify interface.

}
