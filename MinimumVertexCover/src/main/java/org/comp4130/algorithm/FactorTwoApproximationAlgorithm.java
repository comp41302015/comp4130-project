/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.comp4130.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 *
 * @author jonathan
 */
public class FactorTwoApproximationAlgorithm {

    public Set<String> apply(Graph<String, DefaultEdge> t) {
        Set<String> C = new HashSet<>();
        DefaultEdge e;

        while ((e = getEdge(t)) != null) {
            String v1 = t.getEdgeSource(e);
            String v2 = t.getEdgeTarget(e);

            C.add(v1);
            C.add(v2);

            t.removeVertex(v1);
            t.removeVertex(v2);
        }

        return C;
    }

    /**
     * Returns the first accessible edge in the graph t. Returns null if there are
     * no edges in the t.
     * @param t The graph from which to take an edge.
     * 
     * return An arbitrary edge from t, or null if t has no edges.
     */
    private DefaultEdge getEdge(Graph<String, DefaultEdge> t) {
        Set<DefaultEdge> eSet = t.edgeSet();
        List<DefaultEdge> eList = new ArrayList<>();

        if (eSet.isEmpty()) {
            return null;
        }

        eList.addAll(eSet);
        return eList.get(0);
    }

    public static void main(String[] args) {
        FactorTwoApproximationAlgorithm alg = new FactorTwoApproximationAlgorithm();
        Graph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
        final int numVertices = 7;

        String[] s = new String[numVertices];

        for (int i = 0; i < numVertices; i++) {
            s[i] = "v" + (i + 1);
            g.addVertex(s[i]);
        }

        for (int i = 0; i < numVertices; i++) {
            for (int j = i + 1; j < numVertices; j++) {
                g.addEdge(s[i], s[j]);
            }
        }
  //*/      

        for (String v : alg.apply(g)) {
            System.out.print(v + " ");
        }
        System.out.println();
    }
}
