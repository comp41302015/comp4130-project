
package org.comp4130;

/**
 * This class contains the main method; reads command line arguments runs all the
 * algorithms and aggregates results.
 *
 * @author Jonathon Hope
 */
public class Runner {

    /**
     * Program entry point.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {

        System.out.println("Everything is fine!");

    }

}
