/**
 * Copyright(C) 2015 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package org.comp4130.util;

import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.jgrapht.Graph;

import java.util.HashSet;
import java.util.Set;

/**
 * The utility class provides a set of static methods that are useful
 * throughout the rest of the project.
 *
 * @author Jonathon Hope
 */
public final class Utility {

    // prevent instantiation.
    private Utility() {
    }

    /**
     * Checks if the specified vertex set covers every edge of the graph. Uses
     * the definition of Vertex Cover - removes every edge that is incident on a
     * vertex in vertexSet. If no edges are left, vertexSet is a vertex cover
     * for the specified graph.
     *
     * @param vertexSet the vertices to be tested for covering the graph.
     * @param g         the graph to be covered.
     * @return {@code true} if the {@param vertexSet} is a cover of the graph {@param g}
     * and {@code false} otherwise.
     */
    public static boolean isCover(Set<Vertex> vertexSet, Graph<Vertex, Edge> g) {
        Set<Edge> uncoveredEdges = new HashSet<>(g.edgeSet());

        for (Vertex i : vertexSet)
            uncoveredEdges.removeAll(g.edgesOf(i));

        return uncoveredEdges.size() == 0;
    }

}
