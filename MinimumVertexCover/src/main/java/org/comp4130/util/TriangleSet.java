/**
 * Copyright(C) 2015 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package org.comp4130.util;

import org.comp4130.graph.Vertex;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A data structure representing a set of 3 vertices. This is envisioned to be useful
 * to provide utility methods that may help when working with {@link org.comp4130.reduction.impl.TriangleRule}.
 *
 * @author Jonathon Hope
 */
public class TriangleSet {

    private Set<Vertex> vertices;

    /**
     * Constructor.
     *
     * Each Vertex must be distinct from the others.
     */
    protected TriangleSet(Vertex v1, Vertex v2, Vertex v3) {
        vertices = new HashSet<>(3);
        vertices.add(v1);
        vertices.add(v2);
        vertices.add(v3);
    }

    /**
     * Factory method.
     */
    public static TriangleSet create(Vertex v1, Vertex v2, Vertex v3) {
        return new TriangleSet(v1, v2, v3);
    }

    /**
     * @return an iterator for the vertices in this TriangleSet.
     */
    public Iterator<Vertex> getVertices() {
        return vertices.iterator();
    }

}
