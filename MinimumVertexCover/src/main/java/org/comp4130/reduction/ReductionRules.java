package org.comp4130.reduction;

import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.comp4130.reduction.impl.*;
import org.jgrapht.Graph;

/**
 * An enumerated list of approximation preserving reduction rules,
 * provides a facade for choosing the appropriate class.
 *
 * Example Usage:
 * <pre>{@code
 * Graph graph = createRandomGraph();
 *
 * // exhaustively apply reduction rules
 * for ( ReductionStrategy strategy : ReductionRules.values()) {
 *     graph = strategy.execute(graph);
 * }
 *
 * }</pre>
 *
 *
 * @author Jonathon Hope
 */
public enum ReductionRules implements ReductionStrategy {

    DEGREE_RULE_0 {
        @Override
        public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph) {
            return new DegreeRule0().execute(graph);
        }
    },

    DEGREE_RULE_1 {
        @Override
        public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph) {
            return new DegreeRule1().execute(graph);
        }
    },

    DEGREE_RULE_2 {
        @Override
        public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph)  {
            return new DegreeRule2().execute(graph);
        }
    },

    TRIANGLE_RULE {
        @Override
        public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph)  {
            return new TriangleRule().execute(graph);
        }
    },

    AP_DEGREE_RULE_2 {
        @Override
        public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph)  {
            return new ApproximationPreservingDegreeRule2().execute(graph);
        }
    },

    AP_DEGREE_RULE_3 {
        @Override
        public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph)  {
            return new ApproximationPreservingDegreeRule3().execute(graph);
        }
    },

}
