package org.comp4130.reduction;


import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.jgrapht.Graph;

/**
 * An adaption of the strategy pattern to approaching reductions.
 * It may be possible to factor out common default methods.
 *
 * @author Jonathon Hope
 */
public interface ReductionStrategy {

    public Graph<Vertex,Edge> execute(Graph<Vertex,Edge> graph) ;
}
