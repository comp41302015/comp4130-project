/**
 * Copyright(C) 2015 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package org.comp4130.reduction.impl;

import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.comp4130.reduction.ReductionStrategy;
import org.jgrapht.Graph;

/**
 * TODO
 *
 * @author Jonathon Hope
 */
public class ApproximationPreservingDegreeRule3 implements ReductionStrategy {

    @Override
    public Graph<Vertex, Edge> execute(Graph<Vertex, Edge> graph) {
        throw new UnsupportedOperationException();
    }

}
