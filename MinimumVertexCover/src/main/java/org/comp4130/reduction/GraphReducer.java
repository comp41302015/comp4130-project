/**
 * Copyright(C) 2015 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package org.comp4130.reduction;

import org.comp4130.graph.Edge;
import org.comp4130.graph.Vertex;
import org.jgrapht.Graph;

/**
 * A small class that exposes a convenience method for applying all
 * reduction rules to a graph.
 *
 * @author Jonathon Hope
 */
public class GraphReducer {

    /**
     * Exhaustively applies all reduction rules.
     *
     * @param graph a graph to reduce.
     * @return a graph instance resultant from applying all reductions.
     */
    public static Graph<Vertex, Edge> reduce(Graph<Vertex, Edge> graph) {
        for (ReductionStrategy strategy : ReductionRules.values()) {
            graph = strategy.execute(graph);
        }
        return graph;
    }

}
